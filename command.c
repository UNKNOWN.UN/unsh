#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include "sh.h"

extern pid_t waitpid(pid_t pid, int *statloc , int options);

/*
 * Function
 * Name       : check_arg
 * Usage      : check argument is valid
 * Return     : int
 * Parameters : char* argv[]
              : const char* option
*/
int check_arg(char* argv[], const char* option)
{
  int count = 0;

  while(*argv != NULL)
  {
    if (!strcmp(argv[count], option))
    {
      return TRUE;
    }

    ++argv;
  }

  return FALSE;
}

/*
 * Function
 * Name       : cmd_cd
 * Usage      : cd command
 * Return     : void
 * Parameters : int argc
              : char* argv[]
*/
void cmd_cd(int argc, char* argv[])
{
  char* path;

  if (argc > 1)
  {
    path = argv[1];
  }

  else if ((path = (char*)getenv("HOME")) == NULL)
  {
    path = ".";
  }

  if (chdir(path) == ERROR)
  {
    fprintf(stderr, "%s: There is no such file or directory.\n", path);
  }
}

/*
 * Function
 * Name       : cmd_exit
 * Usage      : exit command
 * Return     : void
 * Parameters : void
*/
void cmd_exit()
{
  exit(0);
}

void cmd_showjobs(int argc, char* argv[])
{
  if (argc == 2 && !strcmp(argv[1], "-c"))
    clearjobs();

  else
    showjobs();
}

void cmd_fg(int argc, char* argv[])
{
  int pid, pgid;
  int stat;
  if (argc == 2)
  {
    pid = get_pid_by_job_num(atoi(argv[1]));

    if (pid == -1)
      printf("invaild job number.\n");

    pgid = getpgid(pid);

    if (tcsetpgrp(STDOUT_FILENO, pgid) != 0)
      printf("%d",errno);

    waitpid(pid, &stat, 0);
  }

  else
    printf("input the job number\n");
}

void cmd_bg(int argc, char* argv[])
{
  int count = countbg();
  if (count == 0)
    printf("bg: no such job.\n");
  else
    printf("bg: job %d in background\n", count);
}
