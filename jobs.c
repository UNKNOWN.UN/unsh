#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#include "sh.h"

typedef struct job_struct {
  char cmdstring[20];
  pid_t pgid;
  int job_n;
  job_status_t jobstat;
  struct job_struct* next;
} joblist_t;

static joblist_t* jobhead = NULL;
static joblist_t* jobtail = NULL;

/*
 * Function
 * Name       : cal_job_n
 * Usage      : calculate total number of jobs
 * Return     : int
 * Parameters : 
*/
int cal_job_n()
{
  int count = 0;
  joblist_t* temp;
  if(jobhead == NULL)
    return count;

  temp = jobhead;
  while(temp != NULL)
  {
    temp->job_n = count;
    count++;
    temp = temp->next;
  }

  return count;
}

/*
 * Function
 * Name       : add_job
 * Usage      : add job into list
 * Return     : int
 * Parameters : pid_t pid
 *              char* cmd
 *              job_status_t status
*/
int add_job(pid_t pid, char* cmd, job_status_t status)
{
  joblist_t* job = (joblist_t*)malloc(sizeof(joblist_t));
  if (job == NULL)
    return -1;

  job->pgid = pid;
  strcpy(job->cmdstring, cmd);
  job->jobstat = status;
  job->next = NULL;

  if (jobhead == NULL)
  {
    job->job_n = 0;
    jobhead = job;
    jobtail = job;
  }

  else
  {
    job->job_n = cal_job_n();
    jobtail->next = job;
    jobtail = job;
  }

  return job->job_n;
}

/*
 * Function
 * Name       : delete
 * Usage      : remove job in list
 * Return     : int
 * Parameters : int job
*/
int delete(int job)
{
  joblist_t* temp = jobhead;
  joblist_t* prv = NULL;

  if (job >= cal_job_n() && job < 0)
    return -1;

  if (temp == NULL)
    return -1;

  if (jobhead->job_n == job)
  {
    prv = temp->next;
    jobhead = prv;
    free(temp);
    cal_job_n();
    return job;
  }

  prv = temp;
  temp = temp->next;

  while (temp != NULL)
  {
    if (temp->job_n == job)
    {
      if (temp == jobtail)
        jobtail = prv;
      prv->next = temp->next;
      free(temp);
      cal_job_n();
      return job;
    }

    prv = temp;
    temp = temp->next;
  }

  return -1;
}

/*
 * Function
 * Name       : check jobs
 * Usage      : check jobs for print
 * Return     : void
 * Parameters : 
*/
void checkjobs()
{
  int check;
  int c;
  joblist_t* temp = jobhead;

  while(temp != NULL)
  {
    if (temp->jobstat == BACKGROUND)
      c = waitpid(temp->pgid, &check, WNOHANG);

    if (c != 0)
    {
      temp->jobstat = TERMINATED;
    }

    temp = temp->next;
  }
}

/*
 * Function
 * Name       : showjobs
 * Usage      : print jobs
 * Return     : void
 * Parameters : 
*/
void showjobs()
{
  joblist_t* temp = jobhead;

  checkjobs();

  printf("[Jobs]\t[PID]\t[NAME]\t[STATUS]\n");

  while(temp != NULL)
  {
    printf("[%d]\t%d\t%s\t",temp->job_n, temp->pgid, temp->cmdstring);

    switch (temp->jobstat) {
      case FOREGROUND: printf("Foreground\n"); break;
      case BACKGROUND: printf("Background\n"); break;
      case STOPPED: printf("Stopped\n"); break;
      case DONE: printf("Done\n"); break;
      case TERMINATED:printf("Terminated\n"); break;
    }

    temp = temp->next;
  }
}

/*
 * Function
 * Name       : clearjobs
 * Usage      : remove all jobs
 * Return     : void
 * Parameters : 
*/
void clearjobs()
{
  joblist_t* temp = jobhead;

  while(temp != NULL)
  {
    if (temp->jobstat == TERMINATED)
    {
      delete(temp->job_n);
      cal_job_n();
    }
    temp = temp->next;
  }
  showjobs();
}

/*
 * Function
 * Name       : countbg
 * Usage      : count number of background jobs
 * Return     : int
 * Parameters : 
*/
int countbg()
{
  int count = 0;
  joblist_t* temp = jobhead;

  while(temp != NULL)
  {
    if (temp->jobstat == BACKGROUND)
      count++;
    temp = temp->next;
  }
  return count;
}

/*
 * Function
 * Name       : get_pid_by_job_num
 * Usage      : get pid using job num
 * Return     : pid_t
 * Parameters : int job
*/
pid_t get_pid_by_job_num(int job)
{
  joblist_t* temp = jobhead;

  if (temp->job_n == job)
    return temp->pgid;

  while(temp != NULL)
  {
    if (temp->job_n == job)
      return temp->pgid;

    temp = temp->next;
  }

  return -1;
}
