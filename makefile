custom_shell : shell.o exec.o parse.o symbol.o util.o command.o redirect.o jobs.o
	gcc -o custom_shell shell.o exec.o parse.o symbol.o util.o command.o redirect.o jobs.o -lreadline

shell.o : sh.h shell.c
	gcc -c shell.c -lreadline

jobs.o : sh.h jobs.c
		gcc -c jobs.c

exec.o : sh.h exec.c
	gcc -c exec.c

parse.o : sh.h parse.c
	gcc -c parse.c

symbol.o : sh.h symbol.c
	gcc -c symbol.c

util.o : sh.h util.c
	gcc -c util.c

command.o : sh.h command.c
	gcc -c command.c

redirect.o : sh.h redirect.c
	gcc -c redirect.c

clean :
	rm *.o
	rm custom_shell