#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "sh.h"

/*
 * Function
 * Name       : execute
 * Usage      : execute command using execvp function
 * Return     : int
 * Parameters : int argc
 			  : char* argv[]
			  : int sourcefd
			  : char* sourcefile
			  : int destfd
			  : char* destfile
			  : BOOLEAN append
			  : BOOLEAN backgrnd
*/
int execute (int argc, char* argv[], int sourcefd, char* sourcefile, int destfd, char* destfile, BOOLEAN append, BOOLEAN background)
{
	int	pid;

	if (argc == 0 || shellcmd(argc, argv, sourcefd, destfd))
	{
		return 0;
	}

	pid = fork();

	switch (pid)
	{
		case ERROR :
			fprintf(stderr, "Cannot create new process.\n");
			return 0;

		case 0 :
			redirect(sourcefd, sourcefile, destfd, destfile, append, background);
			execvp(argv[0], argv);
			fprintf(stderr, "%s: Can't find command\n", argv[0]);
			exit(0);

		default :
			if (sourcefd > 0 && close(sourcefd) == ERROR)
			{
				syserr("close sourcefd");
			}

			if (destfd > 1 && close(destfd) == ERROR)
			{
				syserr("close destfd");
			}

			if (background)
			{
				add_job(pid, argv[0], BACKGROUND);
				printf("[%d]\t%d\n", cal_job_n() - 1, pid);
			}

			return pid;
	}
}
