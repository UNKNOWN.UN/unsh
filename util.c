#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "sh.h"

extern char* get_current_dir_name();

/*
 * Function
 * Name       : fatal
 * Usage      : program error
 * Return     : void
 * Parameters : char* message
*/
void fatal(char* message)
{
  fprintf(stderr, "Error: %s\n", message);
  exit(2);
}

/*
 * Function
 * Name       : syserr
 * Usage      : system error
 * Return     : void
 * Parameters : char* message
*/
void syserr(char* message)
{
  fprintf(stderr, "Error: %s (%d", message, errno);
  exit(1);
}

/*
 * Function
 * Name       : waitfor
 * Usage      : wait process until it's finished
 * Return     : void
 * Parameters : int pid
*/
void waitfor(int pid)
{
  int wpid, status;

  while((wpid = wait(&status)) != pid && wpid != ERROR);
}

/*
 * Function
 * Name       : get_prompt
 * Usage      : get the prompt of shell
 * Return     : void
 * Parameters : char* prompt
*/
void get_prompt(char* prompt)
{
  char hostname[40];

  if (gethostname(hostname, sizeof(hostname)))
  {
    syserr("hostname");
  }

  sprintf(prompt, "%s@%s:%s$ ", getenv("USER"), hostname, get_current_dir_name());
}

/*
 * Function
 * Name       : shellcmd
 * Usage      : execute shell built in function
 * Return     : BOOLEAN(int)
 * Parameters : int argc
              : char* argv[]
              : int sourcefd
              : int destfd
*/
BOOLEAN shellcmd(int argc, char* argv[], int sourcefd, int destfd)
{
  char* path;

  if (!strcmp(argv[0], "cd"))
  {
    cmd_cd(argc, argv);
  }

  else if (!strcmp(argv[0], "exit"))
  {
    cmd_exit();
  }

  else if (!strcmp(argv[0], "jobs"))
  {
    cmd_showjobs(argc, argv);
  }

  else if (!strcmp(argv[0], "bg"))
  {
    cmd_bg(argc, argv);
  }

  else if (!strcmp(argv[0], "fg"))
  {
    cmd_fg(argc, argv);
  }

  else
  {
    return FALSE;
  }

  if (sourcefd != 0 || destfd != 1)
  {
    fprintf(stderr, "Illegal redirection or pipeline.\n");
  }

  return TRUE;
}
