#ifndef 	__SH_H__
#define 	__SH_H__

#define 	ERROR 		(-1)
#define 	BADFD 		(-2)

#define 	MAXFNAME 	10
#define 	MAXARG		10
#define 	MAXWORD		20
#define 	MAXFD		  20
#define 	MAXVAR		50
#define 	MAXNAME		20

#define 	TRUE		1
#define 	FALSE		0

#include <sys/types.h>

typedef 	int 		BOOLEAN;

typedef enum
{
	S_WORD,
	S_BAR,
	S_AMP,
	S_SEMI,
	S_GT,
	S_GTGT,
	S_LT,
	S_NL,
	S_EOF
}	SYMBOL;

typedef enum jstatus
{
  FOREGROUND, BACKGROUND, STOPPED, DONE, TERMINATED
} job_status_t;

SYMBOL parse (int* waitpid, BOOLEAN makepipe, int* pipefdp);
SYMBOL getsymbol(char* word);
int add_job(pid_t pid, char* cmd, job_status_t status);
void showjobs();
int delete(int job);
void cmd_showjobs(int argc, char* argv[]);
int execute (int ac, char* av[], int sourcefd, char* sourcefile, int destfd, char* destfile, BOOLEAN append, BOOLEAN backgrnd);
void redirect(int sourcefd, char* sourcefile, int destfd, char* destfile, BOOLEAN append, BOOLEAN backgrnd);
void waitfor(int pid);
void fatal(char* message);
void syserr(char* message);
BOOLEAN shellcmd(int ac, char* av[], int sourcefd, int destfd);
void cmd_cd(int ac, char* av[]);
void cmd_exit();
void get_prompt(char* prompt);
void clearjobs();
void cmd_fg(int argc, char* argv[]);
void cmd_bg(int argc, char* argv[]);
int countbg();
int cal_job_n();
pid_t get_pid_by_job_num(int job);

#endif
