#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "sh.h"

/*
 * Function
 * Name       : sig_handler
 * Usage      : handle the SIGINT(ctrl+c) signal not to shutdown the shell.
 * Return     : void
 * Parameters : int signo (signal number) ** NOT USING **
*/
void sig_handler(int signo)
{
  //DO NOTHING
}

/*
 * Function
 * Name       : stringToStdin
 * Usage      : Input string to stdin.
 * Return     : void
 * Parameters : char* message
*/
void stringToStdin(char* message)
{
  int i;
  int length = strlen(message);
  message[length] = '\n';

  for (i = length; i >= 0; i--)
  {
    ungetc(message[i], stdin);
  }
}

/*
 * Function
 * Name       : main
 * Usage      : program entry point
 * Return     : int
 * Parameters : void
*/
int main()
{
  int pid, fd;
  SYMBOL term;
  char* input;
  char prompt[100];

  signal(SIGINT, (void*)sig_handler);
  rl_bind_key('\t', rl_complete); // set 'tab' key as auto complete

  while (TRUE)
  {
    get_prompt(prompt);
    input = readline(prompt);

    if (!input)
        break;

    add_history(input);

    stringToStdin(input);

    term = parse(&pid, FALSE, NULL);

    if (term != S_AMP && pid != 0) //foreground process
    {
      waitfor(pid);
    }

    for (fd = 3; fd < MAXFD; fd++)
    {
      close(fd); //close all files to clean-up.
    }

    free(input); // input must be freed after use.
  }

  return 0;
}
